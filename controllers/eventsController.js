const Events = require('../models/eventModel.js')
const UserEvents = require('../models/user_events.js')

const api = {}


api.list = (req, res) => {
	Events.findAll().then(event => {

		if (event) {
			return res.json({ respuesta: true, data: event })
		} else {
			return res.json({ respuesta: false, error: "Error al obtener los datos" })
		}

	})
}

api.create = (req, res) => {

	if (!("sTema" in req.body) || !("sPonente" in req.body) || !("sFecha" in req.body) || !("sHora" in req.body) || !("sDireccion" in req.body) || !("sAgenda" in req.body)) {
		return res.json({
			respuesta: false,
			error: "Faltan datos"
		})
	}

	Events.create({
		sTema: req.body.sTema,
		sFecha: req.body.sFecha,
		sHora: req.body.sHora,
		sDireccion: req.body.sDireccion,
		sAgenda: req.body.sAgenda,
		sPonente: req.body.sPonente,
		id_user:0

	}).then(event => {

		if (event) {

			return res.json({
				respuesta: true,
				msj: "Evento: "+req.body.sTema+" creado con éxito" ,
				data: event
			})

		} else {
			return res.json({
				respuesta: false,
				error: "Error al guardar datos"
			})
		}

	})
}

api.enroll = (req, res) => {

	if (!("nIdUser" in req.body) || !("nIdEvent" in req.body)) {
		return res.json({
			respuesta: false,
			error: "Faltan datos"
		})
	}

	UserEvents.create({
		id_user: req.body.nIdUser,
		id_event: req.body.nIdEvent,

	}).then(userEvent => {

		if (userEvent) {
			return res.json({
				respuesta: true,
				msj: "inscrito al evento con éxito" ,
				data: userEvent
			})
		} else {
			return res.json({
				respuesta: false,
				error: "Error al guardar datos"
			})
		}

	})
}

api.delete = (req, res) => {

	if (!("nId" in req.body)) {
		return res.json({
			respuesta: false,
			error: "Faltan datos"
		})
	}

	Events.destroy({
		where: {
			id: req.body.nId
		}
	}).then(eventDelete => {
		console.log("DELETE - - ->", eventDelete)
		if (eventDelete) {

			return res.json({
				respuesta: true,
				msj: "Evento eliminado"
			})

		} else {
			return res.json({
				respuesta: false,
				error: "No se ha podido eliminar o no existe"
			})
		}
	})
}

api.edit = (req, res) => {

	if (!("nId" in req.body) || !("sTema" in req.body) || !("sPonente" in req.body) || !("sFecha" in req.body) || !("sHora" in req.body) || !("sDireccion" in req.body) || !("sAgenda" in req.body)){

		return res.json({ respuesta: false, error: "Faltan datos" })
	}

	Events.findOne({ where: { id: req.body.nId } }).then(event => {

		if (event) {

			event.sTema = req.body.sTema
			event.sFecha = req.body.sFecha
			event.sHora = req.body.sHora
			event.sDireccion = req.body.sDireccion
			event.sAgenda = req.body.sAgenda
			event.sPonente = req.body.sPonente

			event.save()
			return res.json({ respuesta: true, msj: "Datos editados" , data: event})
		} else {
			return res.json({ respuesta: false, error: "No se guardo la información" })
		}
	})
}

api.getEventById = (req, res) => {

	if (!("nId" in req.body)) {
		return res.json({
			respuesta: false,
			error: "Faltan datos"
		})
	}

	Events.findOne({
		where: {
			id: req.body.nId
		}
	}).then(event => {

		if (event) {
			return res.json({
				respuesta: true,
				data: {
					event:event
				}
			})
		} else {
			return res.json({
				respuesta: false,
				data: 'evento no encontrado'
			})
		}
	})
}

module.exports = api