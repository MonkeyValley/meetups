const zookeeper = require('zookeeper-cluster-client')
const Request = require('../models/requestsModel.js')



const api = {}

api.list = (req, res) => {
	Request.findAll().then(request => {

		if (request) {
			return res.json({ respuesta: true, data: request })
		} else {
			return res.json({ respuesta: false, error: "Error al obtener los datos" })
		}

	})
}


api.create = (req, res) => {

	if (!("sNombreContacto" in req.body) || !("cCorreo" in req.body) || !("cTituloCharla" in req.body) || !("cDescCharla" in req.body) || !("cBioUsuario" in req.body) || !("aRedesSociales" in req.body)){
		return res.json({ respuesta: false, error: "Faltan datos" })
	}

	Request.create({nombre:req.body.sNombreContacto, correo:req.body.cCorreo, titulo: req.body.cTituloCharla, descripcion: req.body.cDescCharla, biografia: req.body.cBioUsuario, redesSociales: req.body.aRedesSociales}).then(ingredient => {

		if (ingredient) {
			return res.json({ respuesta: true, msj: "Guardado con exito!" })
		} else {
			return res.json({ respuesta: false, error: "Error al guardar datos" })
		}

	})
}

api.getRequestById = (req, res) => {

	if (!("nId" in req.body)) {
		return res.json({
			respuesta: false,
			error: "Faltan datos"
		})
	}

	Request.findOne({
		where: {
			id: req.body.nId
		}
	}).then(request => {

		if (request) {
			return res.json({
				respuesta: true,
				data: {
					event:request
				}
			})
		} else {
			return res.json({
				respuesta: false,
				data: 'evento no encontrado'
			})
		}
	})
}

module.exports = api