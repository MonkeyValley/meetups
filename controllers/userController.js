const User = require('../models/userModel.js')
const UserEvents = require('../models/user_events.js')

const api = {}

api.login = (req, res) => {

	let text = "";
	const possible = "abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			  
	for (let i = 0; i < 51; i++){
		 text += possible.charAt(Math.floor(Math.random() * possible.length));
	}

	if (!('cEmail' in req.body) || !('cPassword' in req.body)) {
		return res.json({ error: 'Faltan parametros' })
	}

	console.log("CONEXION!")

	User.findOne({ where: { email: req.body.cEmail, pass:req.body.cPassword } }).then(user => {
		if (user) {

			userRes = {
					id: user.id,
					email: user.email,
					token: text,
					type: user.type,
					name: user.name
				}

				user.token = text
				user.save()
				res.json({respuesta:true, data:userRes})
			
		} else{
			res.json({respuesta:false, error: 'Usuario/Contraseña Incorrecta' })
		}
	}).catch(err => {
		res.json({ respuesta:false, error: 'Usuario/Contraseña Incorrecta' })
	})
}
api.userEvents = (req, res) => {

	if (!('idUser' in req.body)) {
		return res.json({ error: 'Faltan parametros' })
	}

	UserEvents.findAll({ where: {id_user:req.body.idUser}}).then(event => {

		if (event) {
			return res.json({ respuesta: true, data: event })
		} else {
			return res.json({ respuesta: false, error: "Error al obtener los datos" })
		}

	})
}
api.register = (req, res) => {


	if (!('cPassword' in req.body) || !('cEmail' in req.body) || !('cName' in req.body) || !('nType' in req.body) ) {
		return res.json({ error: 'Faltan parametros' })
	}
	User.findOne({ where: { email: req.body.cEmail } }).then(user => {

		if(user){
			return res.json({ respuesta: false, error: "Ya hay un usuario registrado con el correo: " + req.body.cEmail})
		}else{
			User.create({name:req.body.cName,  pass:req.body.cPassword, email: req.body.cEmail, type:req.body.nType}).then(user => {

				if (user) {
					return res.json({ respuesta: true, msj: "Usuario registrado con éxito" })
				} else {
					return res.json({ respuesta: false, error: "Error al guardar datos" })
				}
		
			})
		}

	})
	
}
api.edit = (req, res) => {

	if (!("nId" in req.body) || !("cEmail" in req.body) || !("cName" in req.body) || !("cPhone" in req.body) ){

		return res.json({ respuesta: false, error: "Faltan datos" })
	}

	User.findOne({ where: { id: req.body.nId } }).then(user => {

		if (user) {
			user.email = req.body.cEmail
			user.name = req.body.cName
			user.phone = req.body.cPhone

			user.save()
			return res.json({ respuesta: true, msj: "Datos editados" , data: user})
		} else {
			return res.json({ respuesta: false, error: "No se guardo la información" })
		}
	})
}

module.exports = api