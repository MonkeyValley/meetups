const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()

const userRouter = require('./routes/userRouter.js')
const eventsRouter = require('./routes/eventsRouter.js')
const requestsRouter = require('./routes/requestsRouter.js')

app.use(cors())
app.use(bodyParser.json())

app.use((req, res, next) => {
	let token = req.headers.authorization
	req.userId = token
	next()
})

app.use('/meetup/api/users', userRouter)
app.use('/meetup/api/events', eventsRouter)
app.use('/meetup/api/requests', requestsRouter)

app.listen(2605, function () {
	console.log('2605!')
})