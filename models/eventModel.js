const Sequelize = require('sequelize')
const sequelize = require('./config.js')

const event = sequelize.define('event', {
	
	id:{
		type: Sequelize.INTEGER,
		unique: true,
		autoIncrement: true,
		index: true,
		primaryKey: true,
		get(){
			return this.getDataValue('id')
		}
	},
	id_user: {
		type: Sequelize.INTEGER
	},
	sTema: {
		type: Sequelize.STRING
	},
	sFecha:{
		type:Sequelize.STRING
	},
	sHora: {
		type: Sequelize.STRING
	},
	sDireccion: {
		type: Sequelize.STRING
	},
	sPonente:{
		type: Sequelize.STRING
	},
	sAgenda: {
		type: Sequelize.STRING
	}
}, {
	timestamps: true
})

event.sync({force: false}).then(() => {})
module.exports = event