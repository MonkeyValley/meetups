const Sequelize = require('sequelize')
const sequelize = require('./config.js')

const userEvent = sequelize.define('user_event', {
	
	id:{
		type: Sequelize.INTEGER,
		unique: true,
		autoIncrement: true,
		index: true,
		primaryKey: true,
		get(){
			return this.getDataValue('id')
		}
	},
	id_user: {
		type: Sequelize.BIGINT
	},
	id_event: {
		type: Sequelize.INTEGER
	}
}, {
	timestamps: true
})

userEvent.sync({force: false}).then(() => {})
module.exports = userEvent