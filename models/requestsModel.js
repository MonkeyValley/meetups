const Sequelize = require('sequelize')
const sequelize = require('./config.js')

const Request = sequelize.define('request', {
	
	id:{
		type: Sequelize.INTEGER,
		unique: true,
		autoIncrement: true,
		index: true,
		primaryKey: true,
		get(){
			return this.getDataValue('id')
		}
	},
	nombre: {
		type: Sequelize.STRING
	},
	correo: {
		type: Sequelize.STRING
	},
	titulo: {
		type: Sequelize.STRING
	},
	descripcion: {
		type: Sequelize.STRING
	},
	biografia: {
		type: Sequelize.STRING
	},
	redesSociales: {
		type: Sequelize.STRING
	}

}, {
	timestamps: true
})

Request.sync({force: false}).then(() => {})
module.exports = Request