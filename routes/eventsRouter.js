const express = require('express')
const router = express.Router()

const appsController = require('../controllers/eventsController.js')

router.get('/all', appsController.list)
router.post('/create', appsController.create)
router.post('/edit', appsController.edit)
router.post('/delete', appsController.delete)
router.post('/eventById', appsController.getEventById)
router.post('/enroll', appsController.enroll)

module.exports = router