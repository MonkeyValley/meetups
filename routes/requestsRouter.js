const express = require('express')
const router = express.Router()

const appsController = require('../controllers/requestController.js')

router.get('/all', appsController.list)
router.post('/create', appsController.create)
router.post('/requestById', appsController.getRequestById)


module.exports = router