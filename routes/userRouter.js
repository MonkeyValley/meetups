const express = require('express')
const router = express.Router()

const appsController = require('../controllers/userController.js')

router.post('/login', appsController.login)
router.post('/register', appsController.register)
router.post('/getUserEvents', appsController.userEvents)

module.exports = router